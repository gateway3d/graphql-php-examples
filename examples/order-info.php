#!/usr/bin/env php
<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';

if($argc !== 2)
{
	throw new \Exception("Invalid arg count");
}

$tld = getenv('DEV') ? 'red' : 'net';

/**
 * #1 - Get OAuth access token
 */

$guzzle = new \GuzzleHttp\Client;

$response = $guzzle->request('POST', "https://oauth.custom-gateway.{$tld}/token", [
	'form_params' => [
		'grant_type'	=> 'client_credentials',
		'client_id'		=> getenv('CLIENT_ID'),
		'client_secret' => getenv('CLIENT_SECRET'),
		'scope'			=> 'graphql.core.orders order-it.order-manager.view'
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

$token = $data['access_token'];

/**
 * #2 - Order query
 *
 */

$response = $guzzle->request('POST', "https://graphql.custom-gateway.{$tld}", [
	'headers' => [
		'Authorization' => "Bearer {$token}"
	],

	'json' => [
		'query' => <<<'QUERY'
		query($filter: Json!) {
			core {
				orders(filter: $filter) {
					items {
						id
						ref
						external_ref
						status
						status_name

						customer_name
					}
				}
			}
		}
		QUERY,

		'variables' => [
			'filter' => [
				'external_ref' => $argv[1] 
			]
		]
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

if($data['errors'] ?? null)
{
	throw new \Exception($data['errors'][0]['message']);
}

$orders = $data['data']['core']['orders']['items'];

if(count($orders) > 1)
{
	throw new \Exception("More than one match found, do you need to specify which retailer? order.external_ref not guaranteed to be unique");
}
else if(count($orders) == 0)
{
	throw new \Exception("No orders found");
}
else
{
	echo "Order ID: {$orders[0]['id']}, customer '{$orders[0]['customer_name']}', status: '{$orders[0]['status_name']}'" . PHP_EOL;
}

