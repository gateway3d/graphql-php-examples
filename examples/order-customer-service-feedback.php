#!/usr/bin/env php
<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$tld = getenv('DEV') ? 'red' : 'net';

/**
 * #1 - Get OAuth access token
 */

$guzzle = new \GuzzleHttp\Client;

$response = $guzzle->request('POST', "https://oauth.custom-gateway.{$tld}/token", [
	'form_params' => [
		'grant_type' => 'client_credentials',
		'client_id' => getenv('CLIENT_ID'),
		'client_secret' => getenv('CLIENT_SECRET'),
		'scope' => 'graphql.core.order-customer-service-request-feedback graphql.core.order-customer-service-clear-request-feedback'
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

$token = $data['access_token'];

/**
 * #2 - Submit feedback request
 */

$response = $guzzle->request('POST', "https://graphql.custom-gateway.{$tld}", [
	'headers' => [
		'Authorization' => "Bearer {$token}"
	],

	'json' => [
		'query' => <<<'QUERY'
		mutation($id: ID!, $input: CoreOrderCustomerServiceRequestFeedbackInput!) {
			core {
				orderCustomerServiceRequestFeedback(id: $id, input: $input)
			}
		}
		QUERY,

		'variables' => [
			'id' => 1,

			'input' => [
				'message' => 'Customer says that the item is the wrong colour',

				'attachment' => base64_encode(file_get_contents('/path/to/photo.jpg'))
			]
		]
	]
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

if($data['errors'] ?? null)
{
	throw new \Exception($data['errors'][0]['message']);
}

echo PHP_EOL . "Feedback submitted" . PHP_EOL;

