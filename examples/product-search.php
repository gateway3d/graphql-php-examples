#!/usr/bin/env php
<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$tld = getenv('DEV') ? 'red' : 'net';

/**
 * #1 - Get OAuth access token
 */

$guzzle = new \GuzzleHttp\Client;

$response = $guzzle->request('POST', "https://oauth.custom-gateway.{$tld}/token", [
	'form_params' => [
		'grant_type' => 'client_credentials',
		'client_id' => getenv('CLIENT_ID'),
		'client_secret' => getenv('CLIENT_SECRET'),
		'scope' => 'products.customisable.list'
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

$token = $data['access_token'];

/**
 * #2 - Iterate over all products
 */

$page = 1;

do
{
	$response = $guzzle->request('POST', "https://graphql.custom-gateway.{$tld}", [
		'headers' => [
			'Authorization' => "Bearer {$token}"
		],

		'json' => [
			'query' => <<<'QUERY'
			query($page: Int!, $filter: ProductSearchFilter!) {
				search {
					products(page: $page, count: 1000, filter: $filter) {
						items {
							id
							name

							ecommerce {
								sales_description
							}
						}
					}
				}
			}
			QUERY,

			'variables' => [
				'page' => $page++,

				'filter' => [
					'name' => '*Cotton*'
				]
			]
		],

		'verify' => $tld == 'net'
	]);

	$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

	if($data['errors'] ?? null)
	{
		throw new \Exception($data['errors'][0]['message']);
	}

	$products = $data['data']['search']['products']['items'] ?? [];

	foreach($products as $product)
	{
		echo str_repeat("-", 80) . PHP_EOL . PHP_EOL;
		echo str_pad("ID:", 20) . $product['id'] . PHP_EOL;
		echo str_pad("Name:", 20) . $product['name'] . PHP_EOL;
		echo str_pad("Sales Desc:", 20) . preg_replace("/\R/", " ", $product['ecommerce']['sales_description']) . PHP_EOL;
		echo str_pad("Smartlink:", 20) . "https://g3d-app.com/s/app/acp3_2/en_GB/default.html#p={$product['id']}&r=multi" . PHP_EOL;
		echo PHP_EOL . PHP_EOL;
	}
} while($products);
