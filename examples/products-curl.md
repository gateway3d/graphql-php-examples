# Step 1 - Get OAuth access token

```
$ TOKEN=$(curl -F client_id=$CLIENT_ID -F client_secret=$CLIENT_SECRET -F grant_type=client_credentials -F scope=products.customisable.list https://oauth.custom-gateway.net | jq .access_token -r)
```

# Step 2 - Get first 1000 products

```
$ curl -H"Content-type: application/json" -H"Authorization: Bearer $TOKEN" -d'{"query":"query($page: Int!) { core { products(page: $page, count: 1000) { items { id, name } } } }","variables":{"page":1}}' https://graphql.custom-gateway.net | jq .
```
