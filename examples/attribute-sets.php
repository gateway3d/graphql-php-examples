#!/usr/bin/env php
<?php declare(strict_types=1);

require_once dirname(__DIR__) . '/vendor/autoload.php';

$tld = getenv('DEV') ? 'red' : 'net';

/**
 * #1 - Get OAuth access token
 */

$guzzle = new \GuzzleHttp\Client;

$response = $guzzle->request('POST', "https://oauth.custom-gateway.{$tld}/token", [
	'form_params' => [
		'grant_type' => 'client_credentials',
		'client_id' => getenv('CLIENT_ID'),
		'client_secret' => getenv('CLIENT_SECRET'),
		'scope' => 'graphql.core.product-attribute-sets graphql.core.update-product-attribute-set-attributes'
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

$token = $data['access_token'];

/**
 * #2 - List all attribute sets
 */

$response = $guzzle->request('POST', "https://graphql.custom-gateway.{$tld}", [
	'headers' => [
		'Authorization' => "Bearer {$token}"
	],

	'json' => [
		'query' => <<<'QUERY'
		query {
			core {
				product_attribute_sets(count: 100) {
					items {
						id
						name
					}
				}
			}
		}
		QUERY,

		'variables' => [ ]
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

if($data['errors'] ?? null)
{
	throw new \Exception($data['errors'][0]['message']);
}

$sets = $data['data']['core']['product_attribute_sets']['items'];

if(!isset($sets[0]))
{
	throw new \Exception('No sets available');
}

/**
 * #3 - List all attributes for first set
 */

$response = $guzzle->request('POST', "https://graphql.custom-gateway.{$tld}", [
	'headers' => [
		'Authorization' => "Bearer {$token}"
	],

	'json' => [
		'query' => <<<'QUERY'
		query($id: ID!) {
			core {
				s: product_attribute_set(id: $id) {
					attributes(count: 200) {
						items {
							attribute_id
							group_name
							attribute_name
						}
					}
				}
			}
		}
		QUERY,

		'variables' => [
			'id' => $sets[0]['id']
		]
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

if($data['errors'] ?? null)
{
	throw new \Exception($data['errors'][0]['message']);
}

echo PHP_EOL . "Attributes for set '{$sets[0]['name']}' ({$sets[0]['id']}):" . PHP_EOL . PHP_EOL;

$attributes = $data['data']['core']['s']['attributes']['items'];

foreach($attributes as $attribute)
{
	$name = str_pad("#{$attribute['attribute_id']} {$attribute['group_name']}", 100, " ", STR_PAD_RIGHT);

	echo "\t{$name}: {$attribute['attribute_name']}" . PHP_EOL;
}

/**
 * #4 - Create new attribute
 */

$response = $guzzle->request('POST', "https://graphql.custom-gateway.{$tld}", [
	'headers' => [
		'Authorization' => "Bearer {$token}"
	],

	'json' => [
		'query' => <<<'QUERY'
		mutation($setId: ID!, $attributes: [ProductAttributeInput]!) {
			core {
				r: updateProductAttributeSetAttributes(productAttributeSetId: $setId, attributes: $attributes) {
					attribute_id
					group_name
					attribute_name
				}
			}
		}
		QUERY,

		'variables' => [
			'setId' => $sets[0]['id'],

			'attributes' => [
				[
					'group_name' => 'New Attribute',
					'attribute_name' => 'Value'
				]
			]
		]
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

if($data['errors'] ?? null)
{
	throw new \Exception($data['errors'][0]['message']);
}

echo PHP_EOL . "Created new attribute: " . json_encode($data['data']['core']['r']) . PHP_EOL;

/**
 * #4 - Update attribute
 */

$response = $guzzle->request('POST', "https://graphql.custom-gateway.{$tld}", [
	'headers' => [
		'Authorization' => "Bearer {$token}"
	],

	'json' => [
		'query' => <<<'QUERY'
		mutation($setId: ID!, $attributes: [ProductAttributeInput]!) {
			core {
				r: updateProductAttributeSetAttributes(productAttributeSetId: $setId, attributes: $attributes) {
					attribute_id
					group_name
					attribute_name
				}
			}
		}
		QUERY,

		'variables' => [
			'setId' => $sets[0]['id'],

			'attributes' => [
				[
					'attribute_id' => $data['data']['core']['r'][0]['attribute_id'],
					'group_name' => 'New Attribute',
					'attribute_name' => 'Amended Value'
				]
			]
		]
	],

	'verify' => $tld == 'net'
]);

$data = \GuzzleHttp\json_decode((string)$response->getBody(), true);

if($data['errors'] ?? null)
{
	throw new \Exception($data['errors'][0]['message']);
}

echo PHP_EOL . "Updated attribute: " . json_encode($data['data']['core']['r']) . PHP_EOL;

